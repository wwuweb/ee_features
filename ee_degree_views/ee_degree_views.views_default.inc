<?php
/**
 * @file
 * ee_degree_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ee_degree_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'degree_programs';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Degree Programs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'load_more';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<p>Start a new career or complete a degree with Western\'s flexible degree program options designed for working adults.</p>';
  $handler->display->display_options['header']['area']['format'] = 'clean_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '200',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Field: Location */
  $handler->display->display_options['fields']['field_tr_location']['id'] = 'field_tr_location';
  $handler->display->display_options['fields']['field_tr_location']['table'] = 'field_data_field_tr_location';
  $handler->display->display_options['fields']['field_tr_location']['field'] = 'field_tr_location';
  $handler->display->display_options['fields']['field_tr_location']['label'] = '';
  $handler->display->display_options['fields']['field_tr_location']['element_type'] = '0';
  $handler->display->display_options['fields']['field_tr_location']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tr_location']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_tr_location']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tr_location']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_tr_location']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_tr_location']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_location']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  $handler->display->display_options['fields']['field_tr_location']['delta_offset'] = '0';
  /* Field: Field: Online */
  $handler->display->display_options['fields']['field_is_online']['id'] = 'field_is_online';
  $handler->display->display_options['fields']['field_is_online']['table'] = 'field_data_field_is_online';
  $handler->display->display_options['fields']['field_is_online']['field'] = 'field_is_online';
  $handler->display->display_options['fields']['field_is_online']['label'] = '';
  $handler->display->display_options['fields']['field_is_online']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_is_online']['alter']['text'] = 'Online';
  $handler->display->display_options['fields']['field_is_online']['element_type'] = '0';
  $handler->display->display_options['fields']['field_is_online']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_is_online']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_is_online']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_is_online']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_is_online']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_is_online']['type'] = 'list_key';
  $handler->display->display_options['fields']['field_is_online']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'degree_programs' => 'degree_programs',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $export['degree_programs'] = $view;

  $view = new view();
  $view->name = 'degree_programs_other';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Degree Programs - Other';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = '[field_program_finder_title]';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Program Finder Data (field_program_finder_data) */
  $handler->display->display_options['relationships']['field_program_finder_data_value']['id'] = 'field_program_finder_data_value';
  $handler->display->display_options['relationships']['field_program_finder_data_value']['table'] = 'field_data_field_program_finder_data';
  $handler->display->display_options['relationships']['field_program_finder_data_value']['field'] = 'field_program_finder_data_value';
  $handler->display->display_options['relationships']['field_program_finder_data_value']['label'] = 'Field Collection';
  $handler->display->display_options['relationships']['field_program_finder_data_value']['delta'] = '-1';
  /* Field: Field collection item: Program Finder External URL */
  $handler->display->display_options['fields']['field_program_finder_external_ur']['id'] = 'field_program_finder_external_ur';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['table'] = 'field_data_field_program_finder_external_ur';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['field'] = 'field_program_finder_external_ur';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['relationship'] = 'field_program_finder_data_value';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['label'] = '';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_program_finder_external_ur']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_external_ur']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_external_ur']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_program_finder_external_ur']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['element_default_classes'] = FALSE;
  /* Field: Custom: URL/Link */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Custom: URL/Link';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_program_finder_external_ur]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['nothing']['empty'] = '[path]';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[nothing]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Field collection item: Program Finder Title */
  $handler->display->display_options['fields']['field_program_finder_title']['id'] = 'field_program_finder_title';
  $handler->display->display_options['fields']['field_program_finder_title']['table'] = 'field_data_field_program_finder_title';
  $handler->display->display_options['fields']['field_program_finder_title']['field'] = 'field_program_finder_title';
  $handler->display->display_options['fields']['field_program_finder_title']['relationship'] = 'field_program_finder_data_value';
  $handler->display->display_options['fields']['field_program_finder_title']['label'] = '';
  $handler->display->display_options['fields']['field_program_finder_title']['element_type'] = '0';
  $handler->display->display_options['fields']['field_program_finder_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_title']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_program_finder_title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_title']['empty'] = '[title]';
  $handler->display->display_options['fields']['field_program_finder_title']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_title']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Custom: Title - with link */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'Custom: Title - with link';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '[field_program_finder_title]';
  $handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_1']['alter']['path'] = '[nothing]';
  $handler->display->display_options['fields']['nothing_1']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['nothing_1']['element_type'] = 'h2';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['nothing_1']['empty'] = '[title]';
  /* Field: Field collection item: Program Finder Description */
  $handler->display->display_options['fields']['field_program_finder_description']['id'] = 'field_program_finder_description';
  $handler->display->display_options['fields']['field_program_finder_description']['table'] = 'field_data_field_program_finder_description';
  $handler->display->display_options['fields']['field_program_finder_description']['field'] = 'field_program_finder_description';
  $handler->display->display_options['fields']['field_program_finder_description']['relationship'] = 'field_program_finder_data_value';
  $handler->display->display_options['fields']['field_program_finder_description']['label'] = '';
  $handler->display->display_options['fields']['field_program_finder_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_program_finder_description']['settings'] = array(
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Custom: Title - no link */
  $handler->display->display_options['fields']['nothing_3']['id'] = 'nothing_3';
  $handler->display->display_options['fields']['nothing_3']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_3']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_3']['ui_name'] = 'Custom: Title - no link';
  $handler->display->display_options['fields']['nothing_3']['label'] = '';
  $handler->display->display_options['fields']['nothing_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nothing_3']['alter']['text'] = '[field_program_finder_title]';
  $handler->display->display_options['fields']['nothing_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing_3']['empty'] = '[title]';
  /* Field: Custom: Learn more link */
  $handler->display->display_options['fields']['nothing_2']['id'] = 'nothing_2';
  $handler->display->display_options['fields']['nothing_2']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_2']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_2']['ui_name'] = 'Custom: Learn more link';
  $handler->display->display_options['fields']['nothing_2']['label'] = '';
  $handler->display->display_options['fields']['nothing_2']['alter']['text'] = 'Learn more about [field_program_finder_title]';
  $handler->display->display_options['fields']['nothing_2']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['alter']['path'] = '[nothing]';
  $handler->display->display_options['fields']['nothing_2']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['nothing_2']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Field collection item: Appears in Degree Programs &amp; Courses Page (field_degree_courses_visible) */
  $handler->display->display_options['filters']['field_degree_courses_visible_value']['id'] = 'field_degree_courses_visible_value';
  $handler->display->display_options['filters']['field_degree_courses_visible_value']['table'] = 'field_data_field_degree_courses_visible';
  $handler->display->display_options['filters']['field_degree_courses_visible_value']['field'] = 'field_degree_courses_visible_value';
  $handler->display->display_options['filters']['field_degree_courses_visible_value']['relationship'] = 'field_program_finder_data_value';
  $handler->display->display_options['filters']['field_degree_courses_visible_value']['value'] = array(
    1 => '1',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $export['degree_programs_other'] = $view;

  return $export;
}
