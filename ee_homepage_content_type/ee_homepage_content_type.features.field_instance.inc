<?php
/**
 * @file
 * ee_homepage_content_type.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ee_homepage_content_type_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-homepage-field_hp_bottom'.
  $field_instances['node-homepage-field_hp_bottom'] = array(
    'bundle' => 'homepage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paragraphs',
        'settings' => array(
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_hp_bottom',
    'label' => 'Bottom Content',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        '25_25_25_25_content_blocks' => -1,
        '33_33_33_content_blocks' => -1,
        'accordion' => -1,
        'accordion_item' => -1,
        'content_block' => -1,
        'content_block_w_slider' => -1,
        'image_four_grid_full_width' => -1,
        'image_single_full_width' => -1,
        'image_three_full_width' => -1,
        'image_two_short_full_width' => -1,
        'image_two_tall_full_width' => -1,
        'paragraphs_pack_content' => -1,
        'text_content' => -1,
      ),
      'bundle_weights' => array(
        '25_25_25_25_content_blocks' => 2,
        '33_33_33_content_blocks' => 3,
        'accordion' => 4,
        'accordion_item' => 5,
        'content_block' => 6,
        'content_block_w_slider' => 7,
        'image_four_grid_full_width' => 8,
        'image_single_full_width' => 9,
        'image_three_full_width' => 10,
        'image_two_short_full_width' => 11,
        'image_two_tall_full_width' => 12,
        'paragraphs_pack_content' => 13,
        'text_content' => 14,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Bottom Content',
      'title_multiple' => 'Bottom Content',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-homepage-field_hp_location'.
  $field_instances['node-homepage-field_hp_location'] = array(
    'bundle' => 'homepage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
        ),
        'type' => 'text_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_hp_location',
    'label' => 'Location Text',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-homepage-field_hp_middle'.
  $field_instances['node-homepage-field_hp_middle'] = array(
    'bundle' => 'homepage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paragraphs',
        'settings' => array(
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_hp_middle',
    'label' => 'Middle Content',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        '25_25_25_25_content_blocks' => -1,
        '33_33_33_content_blocks' => -1,
        'accordion' => -1,
        'accordion_item' => -1,
        'content_block' => -1,
        'content_block_w_slider' => -1,
        'image_four_grid_full_width' => -1,
        'image_single_full_width' => -1,
        'image_three_full_width' => -1,
        'image_two_short_full_width' => -1,
        'image_two_tall_full_width' => -1,
        'paragraphs_pack_content' => -1,
        'text_content' => -1,
      ),
      'bundle_weights' => array(
        '25_25_25_25_content_blocks' => 2,
        '33_33_33_content_blocks' => 3,
        'accordion' => 4,
        'accordion_item' => 5,
        'content_block' => 6,
        'content_block_w_slider' => 7,
        'image_four_grid_full_width' => 8,
        'image_single_full_width' => 9,
        'image_three_full_width' => 10,
        'image_two_short_full_width' => 11,
        'image_two_tall_full_width' => 12,
        'paragraphs_pack_content' => 13,
        'text_content' => 14,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Middle Content',
      'title_multiple' => 'Middle Content',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-homepage-field_hp_top'.
  $field_instances['node-homepage-field_hp_top'] = array(
    'bundle' => 'homepage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paragraphs',
        'settings' => array(
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_hp_top',
    'label' => 'Top Content',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        '25_25_25_25_content_blocks' => -1,
        '33_33_33_content_blocks' => -1,
        'accordion' => -1,
        'accordion_item' => -1,
        'content_block' => -1,
        'content_block_w_slider' => -1,
        'image_four_grid_full_width' => -1,
        'image_single_full_width' => -1,
        'image_three_full_width' => -1,
        'image_two_short_full_width' => -1,
        'image_two_tall_full_width' => -1,
        'paragraphs_pack_content' => -1,
        'text_content' => -1,
      ),
      'bundle_weights' => array(
        '25_25_25_25_content_blocks' => 2,
        '33_33_33_content_blocks' => 3,
        'accordion' => 4,
        'accordion_item' => 5,
        'content_block' => 6,
        'content_block_w_slider' => 7,
        'image_four_grid_full_width' => 8,
        'image_single_full_width' => 9,
        'image_three_full_width' => 10,
        'image_two_short_full_width' => 11,
        'image_two_tall_full_width' => 12,
        'paragraphs_pack_content' => 13,
        'text_content' => 14,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Top Content',
      'title_multiple' => 'Top Content',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-homepage-field_image'.
  $field_instances['node-homepage-field_image'] = array(
    'bundle' => 'homepage',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_image',
    'label' => 'Header Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'oembed' => 0,
          'private' => 0,
          'public' => 'public',
          'vimeo' => 0,
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_browser_plus--media_browser_my_files' => 0,
          'media_browser_plus--media_browser_thumbnails' => 0,
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 0,
          'upload' => 0,
        ),
      ),
      'type' => 'media_generic',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-homepage-field_image_1'.
  $field_instances['node-homepage-field_image_1'] = array(
    'bundle' => 'homepage',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
        ),
        'type' => 'image',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_image_1',
    'label' => 'Location Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'oembed' => 0,
          'private' => 0,
          'public' => 'public',
          'vimeo' => 0,
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_browser_plus--media_browser_my_files' => 0,
          'media_browser_plus--media_browser_thumbnails' => 0,
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 0,
          'upload' => 0,
        ),
      ),
      'type' => 'media_generic',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-homepage-field_subtitle'.
  $field_instances['node-homepage-field_subtitle'] = array(
    'bundle' => 'homepage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'linked_field' => array(
            'advanced' => array(
              'class' => '',
              'rel' => '',
              'target' => '',
              'text' => '',
              'title' => '',
            ),
            'destination' => '',
            'linked' => 0,
          ),
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_subtitle',
    'label' => 'Subtitle',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Bottom Content');
  t('Header Image');
  t('Location Image');
  t('Location Text');
  t('Middle Content');
  t('Subtitle');
  t('Top Content');

  return $field_instances;
}
