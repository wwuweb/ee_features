<?php
/**
 * @file
 * ee_programs_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ee_programs_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'programs';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Programs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_grade_school_level_tid' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => 1,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'All',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_session_tid' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => 1,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'All',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_tr_location_tid' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => 1,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'All',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'clean_html';
  $handler->display->display_options['pager']['type'] = 'load_more';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no programs that match your criteria. Please change your criteria and try again.';
  $handler->display->display_options['empty']['area']['format'] = 'clean_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Program Description */
  $handler->display->display_options['fields']['field_program_description']['id'] = 'field_program_description';
  $handler->display->display_options['fields']['field_program_description']['table'] = 'field_data_field_program_description';
  $handler->display->display_options['fields']['field_program_description']['field'] = 'field_program_description';
  $handler->display->display_options['fields']['field_program_description']['label'] = '';
  $handler->display->display_options['fields']['field_program_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_program_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_program_description']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['field_program_description']['settings'] = array(
    'trim_length' => '300',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Field: Location */
  $handler->display->display_options['fields']['field_tr_location']['id'] = 'field_tr_location';
  $handler->display->display_options['fields']['field_tr_location']['table'] = 'field_data_field_tr_location';
  $handler->display->display_options['fields']['field_tr_location']['field'] = 'field_tr_location';
  $handler->display->display_options['fields']['field_tr_location']['label'] = '';
  $handler->display->display_options['fields']['field_tr_location']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_tr_location']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_tr_location']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_location']['delta_offset'] = '0';
  /* Field: Content: Grade/School Level */
  $handler->display->display_options['fields']['field_grade_school_level']['id'] = 'field_grade_school_level';
  $handler->display->display_options['fields']['field_grade_school_level']['table'] = 'field_data_field_grade_school_level';
  $handler->display->display_options['fields']['field_grade_school_level']['field'] = 'field_grade_school_level';
  $handler->display->display_options['fields']['field_grade_school_level']['label'] = '';
  $handler->display->display_options['fields']['field_grade_school_level']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_grade_school_level']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_grade_school_level']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_grade_school_level']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program' => 'program',
  );
  /* Filter criterion: Content: Grade/School Level (field_grade_school_level) */
  $handler->display->display_options['filters']['field_grade_school_level_tid']['id'] = 'field_grade_school_level_tid';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['table'] = 'field_data_field_grade_school_level';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['field'] = 'field_grade_school_level_tid';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['operator_id'] = 'field_grade_school_level_tid_op';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['label'] = 'Grade/School Level';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['operator'] = 'field_grade_school_level_tid_op';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['identifier'] = 'grade_level';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['field_grade_school_level_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['vocabulary'] = 'grade_school_level';
  /* Filter criterion: Content: Session (field_session) */
  $handler->display->display_options['filters']['field_session_tid']['id'] = 'field_session_tid';
  $handler->display->display_options['filters']['field_session_tid']['table'] = 'field_data_field_session';
  $handler->display->display_options['filters']['field_session_tid']['field'] = 'field_session_tid';
  $handler->display->display_options['filters']['field_session_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_session_tid']['expose']['operator_id'] = 'field_session_tid_op';
  $handler->display->display_options['filters']['field_session_tid']['expose']['label'] = 'Session (field_session)';
  $handler->display->display_options['filters']['field_session_tid']['expose']['operator'] = 'field_session_tid_op';
  $handler->display->display_options['filters']['field_session_tid']['expose']['identifier'] = 'session';
  $handler->display->display_options['filters']['field_session_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_session_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_session_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['field_session_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_session_tid']['vocabulary'] = 'session';
  /* Filter criterion: Field: Location (field_tr_location) */
  $handler->display->display_options['filters']['field_tr_location_tid']['id'] = 'field_tr_location_tid';
  $handler->display->display_options['filters']['field_tr_location_tid']['table'] = 'field_data_field_tr_location';
  $handler->display->display_options['filters']['field_tr_location_tid']['field'] = 'field_tr_location_tid';
  $handler->display->display_options['filters']['field_tr_location_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['operator_id'] = 'field_tr_location_tid_op';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['label'] = 'Location';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['operator'] = 'field_tr_location_tid_op';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['identifier'] = 'location';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['field_tr_location_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_tr_location_tid']['vocabulary'] = 'tax_location';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'og_group_ref_target_id' => array(
      'type' => 'panel',
      'context' => 'entity:field_collection_item.field_degree_courses_visible',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Groups audience (og_group_ref)',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '0';

  /* Display: Youth - Content pane */
  $handler = $view->new_display('panel_pane', 'Youth - Content pane', 'panel_pane_2');
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: OG membership: OG membership from Node */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['relationship'] = 'og_membership_rel';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program' => 'program',
  );
  /* Filter criterion: Content: Grade/School Level (field_grade_school_level) */
  $handler->display->display_options['filters']['field_grade_school_level_tid']['id'] = 'field_grade_school_level_tid';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['table'] = 'field_data_field_grade_school_level';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['field'] = 'field_grade_school_level_tid';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['operator_id'] = 'field_grade_school_level_tid_op';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['label'] = 'Grade/School Level';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['operator'] = 'field_grade_school_level_tid_op';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['identifier'] = 'grade_level';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_grade_school_level_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['field_grade_school_level_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['field_grade_school_level_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_grade_school_level_tid']['vocabulary'] = 'grade_school_level';
  /* Filter criterion: Content: Session (field_session) */
  $handler->display->display_options['filters']['field_session_tid']['id'] = 'field_session_tid';
  $handler->display->display_options['filters']['field_session_tid']['table'] = 'field_data_field_session';
  $handler->display->display_options['filters']['field_session_tid']['field'] = 'field_session_tid';
  $handler->display->display_options['filters']['field_session_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_session_tid']['expose']['operator_id'] = 'field_session_tid_op';
  $handler->display->display_options['filters']['field_session_tid']['expose']['label'] = 'Session (field_session)';
  $handler->display->display_options['filters']['field_session_tid']['expose']['operator'] = 'field_session_tid_op';
  $handler->display->display_options['filters']['field_session_tid']['expose']['identifier'] = 'session';
  $handler->display->display_options['filters']['field_session_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_session_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['field_session_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['field_session_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_session_tid']['vocabulary'] = 'session';
  /* Filter criterion: Field: Location (field_tr_location) */
  $handler->display->display_options['filters']['field_tr_location_tid']['id'] = 'field_tr_location_tid';
  $handler->display->display_options['filters']['field_tr_location_tid']['table'] = 'field_data_field_tr_location';
  $handler->display->display_options['filters']['field_tr_location_tid']['field'] = 'field_tr_location_tid';
  $handler->display->display_options['filters']['field_tr_location_tid']['value'] = array(
    7 => '7',
    8 => '8',
    13 => '13',
  );
  $handler->display->display_options['filters']['field_tr_location_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['operator_id'] = 'field_tr_location_tid_op';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['label'] = 'Location';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['operator'] = 'field_tr_location_tid_op';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['identifier'] = 'location';
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['field_tr_location_tid']['expose']['reduce'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['field_tr_location_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_tr_location_tid']['vocabulary'] = 'tax_location';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'gid' => array(
      'type' => 'none',
      'context' => 'entity:node.og-group-ref--og-membership',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'OG membership: Group ID',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '0';

  /* Display: OG - Content pane */
  $handler = $view->new_display('panel_pane', 'OG - Content pane', 'panel_pane_3');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: OG membership: OG membership from Node */
  $handler->display->display_options['relationships']['og_membership_rel']['id'] = 'og_membership_rel';
  $handler->display->display_options['relationships']['og_membership_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['og_membership_rel']['field'] = 'og_membership_rel';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: OG membership: Group ID */
  $handler->display->display_options['arguments']['gid']['id'] = 'gid';
  $handler->display->display_options['arguments']['gid']['table'] = 'og_membership';
  $handler->display->display_options['arguments']['gid']['field'] = 'gid';
  $handler->display->display_options['arguments']['gid']['relationship'] = 'og_membership_rel';
  $handler->display->display_options['arguments']['gid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['gid']['default_argument_type'] = 'og_context';
  $handler->display->display_options['arguments']['gid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['gid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['gid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program' => 'program',
  );
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'gid' => array(
      'type' => 'none',
      'context' => 'entity:node.og-group-ref--og-membership',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'OG membership: Group ID',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '0';
  $export['programs'] = $view;

  return $export;
}
