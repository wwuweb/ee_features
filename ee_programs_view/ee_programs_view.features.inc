<?php
/**
 * @file
 * ee_programs_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ee_programs_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
