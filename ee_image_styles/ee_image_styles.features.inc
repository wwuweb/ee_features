<?php
/**
 * @file
 * ee_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function ee_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: degree_program_image.
  $styles['degree_program_image'] = array(
    'label' => 'Degree Program Image',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 775,
          'height' => 435,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_location.
  $styles['homepage_location'] = array(
    'label' => 'Homepage Location',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 767,
          'height' => 767,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: paragraphs_content_block.
  $styles['paragraphs_content_block'] = array(
    'label' => 'Paragraphs Content Block',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 475,
          'height' => 475,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: paragraphs_full_width_image.
  $styles['paragraphs_full_width_image'] = array(
    'label' => 'Paragraphs Full Width Image',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1060,
          'height' => 580,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: paragraphs_three_large_image.
  $styles['paragraphs_three_large_image'] = array(
    'label' => 'Paragraphs Three Large Image',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 710,
          'height' => 399,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: paragraphs_two_image.
  $styles['paragraphs_two_image'] = array(
    'label' => 'Paragraphs Two Image',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 550,
          'height' => 550,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: paragraphs_two_short_image.
  $styles['paragraphs_two_short_image'] = array(
    'label' => 'Paragraphs Two Short Image',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 550,
          'height' => 310,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: program_comparison_image.
  $styles['program_comparison_image'] = array(
    'label' => 'Program Comparison Image',
    'effects' => array(),
  );

  // Exported image style: program_image.
  $styles['program_image'] = array(
    'label' => 'Program Image',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 510,
          'height' => 287,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: testimonial_profile.
  $styles['testimonial_profile'] = array(
    'label' => 'Testimonial Profile',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 240,
          'height' => 240,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
