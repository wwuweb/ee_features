<?php
/**
 * @file
 * ee_paragraph_bundles.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function ee_paragraph_bundles_paragraphs_info() {
  $items = array(
    '25_25_25_25_content_blocks' => array(
      'name' => 'Content: Blocks w/Image (25/25/25/25)',
      'bundle' => '25_25_25_25_content_blocks',
      'locked' => '1',
    ),
    '33_33_33_content_blocks' => array(
      'name' => 'Content: Blocks w/Image (33/33/33)',
      'bundle' => '33_33_33_content_blocks',
      'locked' => '1',
    ),
    'content_block' => array(
      'name' => 'Content: Block w/Image',
      'bundle' => 'content_block',
      'locked' => '1',
    ),
    'content_block_w_slider' => array(
      'name' => 'Content: Block w/Slider',
      'bundle' => 'content_block_w_slider',
      'locked' => '1',
    ),
    'image_four_grid_full_width' => array(
      'name' => 'Image: Four, grid, full width',
      'bundle' => 'image_four_grid_full_width',
      'locked' => '1',
    ),
    'image_single_full_width' => array(
      'name' => 'Image: Single, full width',
      'bundle' => 'image_single_full_width',
      'locked' => '1',
    ),
    'image_three_full_width' => array(
      'name' => 'Image: Three, full width',
      'bundle' => 'image_three_full_width',
      'locked' => '1',
    ),
    'image_two_short_full_width' => array(
      'name' => 'Image: Two, short, full width',
      'bundle' => 'image_two_short_full_width',
      'locked' => '1',
    ),
    'image_two_tall_full_width' => array(
      'name' => 'Image: Two, tall, full width',
      'bundle' => 'image_two_tall_full_width',
      'locked' => '1',
    ),
    'paragraphs_pack_content' => array(
      'name' => 'Content: with Title',
      'bundle' => 'paragraphs_pack_content',
      'locked' => '1',
    ),
    'text_content' => array(
      'name' => 'Content: No Title',
      'bundle' => 'text_content',
      'locked' => '1',
    ),
  );
  return $items;
}
