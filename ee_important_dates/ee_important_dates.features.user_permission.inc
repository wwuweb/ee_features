<?php
/**
 * @file
 * ee_important_dates.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ee_important_dates_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'delete terms in important_dates'.
  $permissions['delete terms in important_dates'] = array(
    'name' => 'delete terms in important_dates',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in important_dates'.
  $permissions['edit terms in important_dates'] = array(
    'name' => 'edit terms in important_dates',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
