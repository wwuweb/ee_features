<?php
/**
 * @file
 * ee_important_dates.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ee_important_dates_taxonomy_default_vocabularies() {
  return array(
    'important_dates' => array(
      'name' => 'Important Dates',
      'machine_name' => 'important_dates',
      'description' => 'Important dates appear in each section',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
