<?php
/**
 * @file
 * ee_important_dates.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ee_important_dates_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ee_important_dates_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
