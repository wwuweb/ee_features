<?php
/**
 * @file
 * ee_program_finder.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ee_program_finder_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
